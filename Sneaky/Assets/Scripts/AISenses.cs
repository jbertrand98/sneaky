﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISenses : MonoBehaviour
{

    public float viewDistance = 5f; // How far they can see
    public float fieldOfView = 45f; // View angle (Field of View)
    public float hearingScale = 1.0f; // How well they can hear

    private Transform tf;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); //Get this objects transform
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool CanHear(GameObject target)
    {
        NoiseMaker targetNoiseMaker = target.GetComponent<NoiseMaker>(); //Get the target objects noisemaker
        if (targetNoiseMaker == null) //If they have no noisemaker they cant be heard
        {
            return false;
        }

        Transform targetTf = target.GetComponent<Transform>(); //Get target transform
        if (Vector3.Distance(targetTf.position, tf.position) <= targetNoiseMaker.volume * hearingScale) //Check if the distance between the target and this object is less than the range of the noise they are making
        {
            return true;
        }

        return false;
    }


    public bool CanSee(GameObject target)
    {
        Collider2D targetCollider = target.GetComponent<Collider2D>(); //Get the targets collider

        Transform targetTransform = target.GetComponent<Transform>(); //Get the targets transform
        //Get the direction vector pointing from the object to the target
        Vector3 vectorToTarget = targetTransform.position - tf.position;
        vectorToTarget.Normalize();

        if (Vector3.Angle(vectorToTarget, tf.right) >= fieldOfView) //Check to see if the target is in the field of view
        {
            return false;
        }

        RaycastHit2D hitInfo = Physics2D.Raycast(tf.position, vectorToTarget, viewDistance); //Cast a raycast towards the target

        if (hitInfo.collider == null) //if the raycast doesn't hit anything
        {
            return false;
        }

        if (hitInfo.collider == targetCollider) //if the raycast hits the target collider
        {
            return true;
        }

        return false;
    }

}
