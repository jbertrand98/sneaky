﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : Controller
{

    //Enum used for enemy state
    public enum EnemyState
    {
        Idle, //0
        Search, //1
        Chase, //2
        GoHome //3
    }
    public EnemyState currentState;



    // Start is called before the first frame update
    public override void Start()
    {
        base.Start(); //Call parent start function

    }

    // Update is called once per frame
    void Update()
    {

        switch (currentState) //Check current state
        {
            case EnemyState.Idle: 
                currentState = (EnemyState)pawn.Idle(); //Set the state to the return value of pawn.Idle()
                break;

            case EnemyState.Search:
                currentState = (EnemyState)pawn.Search(); //Set the state to the return value of pawn.Search()
                break;

            case EnemyState.Chase:
                currentState = (EnemyState)pawn.Chase(); //Set the state to the return value of pawn.Chase()
                break;

            case EnemyState.GoHome:
                currentState = (EnemyState)pawn.GoHome(); //Set the state to the return value of pawn.GoHome()
                break;

        }


    }
}
