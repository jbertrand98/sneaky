﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Base Controller class
public abstract class Controller : MonoBehaviour
{
    //Generic Pawn Component
    public Pawn pawn;

    // Start is called before the first frame update
    public virtual void Start()
    {
        pawn = GetComponent<Pawn>(); //Get the pawn attached to this GameObject
    }

}
