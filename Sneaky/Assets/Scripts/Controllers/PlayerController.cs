﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller
{

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start(); //Run parents Start function
    }

    // Update is called once per frame
    void Update()
    {
        //Manage player input
        if(Input.GetKey("up") || Input.GetKey("w"))
        {
            pawn.MoveForward();
        }

        if (Input.GetKey("down") || Input.GetKey("s"))
        {
            pawn.MoveBackward();
        }

        if (Input.GetKey("left") || Input.GetKey("a"))
        {
            pawn.TurnLeft();
        }

        if (Input.GetKey("right") || Input.GetKey("d"))
        {
            pawn.TurnRight();
        }

    }

    public void OnCollisionEnter2D(Collision2D collision) //Player collision management
    {
        
        if (collision.gameObject.tag == "Enemy") //Check to see if the collision was with an enemy
        {
            GameManager.instance.SetGameState((int)GameManager.GameState.Lose); //Set game state to lose
        }

    }

}
