﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager instance; //Singleton instance

    //Lists of GameObjects to be manipulated by the Game Manager
    public List<GameObject> states;
    public List<GameObject> images;
    public List<EnemyController> enemyControllers;
    public List<EnemyPawn> enemyPawns;

    //Pawn associated with the player
    public PlayerPawn player;

    public enum GameState //Enum to manage the state of the game
    {
        Home, //0
        Play, //1
        About, //2
        Win, //3
        Lose, //4
        Quit //5
    }
    public GameState gameState;

    public enum DetectionState //Enum to manage the state of the World Space Canvas
    {
        Hidden, //0
        Heard, //1
        Seen //2
    }
    public DetectionState detectionState;



    private void Awake()
    {
        // Setup the singleton
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        //Set the initial states
        SetGameState((int)GameState.Home);
        SetDetectionState((int)DetectionState.Hidden);
    }

    // Update is called once per frame
    void Update()
    {

        if(isSeen()) //If any enemy can see the player
        {
            SetDetectionState((int)DetectionState.Seen); //Update the state of the World Space canvas to state "Seen"
        }
        else if(isHeard()) //If any enemy can hear the player
        {
            SetDetectionState((int)DetectionState.Heard); //Update the state of the World Space canvas to state "Heard"
        }
        else //Otherwise
        {
            SetDetectionState((int)DetectionState.Hidden); //Update the state of the World Space canvas to state "Hidden"
        }

    }

    public void SetGameState(int state) //Function for changing the game state
    {
        gameState = (GameState)state; //Set the game state
        ActivateScreens(); //Change which objects are active based on the new game state
    }

    public void SetDetectionState(int state) //Function for changin the state of the World Space canvas
    {
        detectionState = (DetectionState)state; //Set the state
        ActivateDetectionImage(); //Change which objects are active based on the new state
    }

    void ActivateScreens() //Changes which groups of objects are active based on the current game state
    {

        switch (gameState) //checks the current game state
        {

            case GameState.Home: //Turns on the main menu and turns all other GameObjects off
                states[(int)GameState.Home].SetActive(true);
                states[(int)GameState.Play].SetActive(false);
                states[(int)GameState.About].SetActive(false);
                states[(int)GameState.Win].SetActive(false);
                states[(int)GameState.Lose].SetActive(false);
                break;

            case GameState.Play: //Turns on the main game and turns all other GameObjects off
                states[(int)GameState.Home].SetActive(false);
                states[(int)GameState.Play].SetActive(true);
                states[(int)GameState.About].SetActive(false);
                states[(int)GameState.Win].SetActive(false);
                states[(int)GameState.Lose].SetActive(false);
                break;

            case GameState.About: //Turns on the about screen and turns all other GameObjects off
                states[(int)GameState.Home].SetActive(false);
                states[(int)GameState.Play].SetActive(false);
                states[(int)GameState.About].SetActive(true);
                states[(int)GameState.Win].SetActive(false);
                states[(int)GameState.Lose].SetActive(false);
                break;

            case GameState.Win: //Turns on the win screen and turns all other GameObjects off
                states[(int)GameState.Home].SetActive(false);
                states[(int)GameState.Play].SetActive(false);
                states[(int)GameState.About].SetActive(false);
                states[(int)GameState.Win].SetActive(true);
                states[(int)GameState.Lose].SetActive(false);
                break;

            case GameState.Lose: //Turns on the lose screen and turns all other GameObjects off
                states[(int)GameState.Home].SetActive(false);
                states[(int)GameState.Play].SetActive(false);
                states[(int)GameState.About].SetActive(false);
                states[(int)GameState.Win].SetActive(false);
                states[(int)GameState.Lose].SetActive(true);
                break;

            case GameState.Quit: //Exits the application
                Application.Quit();
                break;

        }

    }

    void ActivateDetectionImage() //Changes which groups of objects are active based on the current state
    {

        switch (detectionState) //Checks the current state
        {

            case DetectionState.Seen: //Turns on the seen indicator
                images[(int)DetectionState.Hidden].SetActive(false);
                images[(int)DetectionState.Heard].SetActive(false);
                images[(int)DetectionState.Seen].SetActive(true);
                break;

            case DetectionState.Heard: //Turns on the heard indicator
                images[(int)DetectionState.Hidden].SetActive(false);
                images[(int)DetectionState.Heard].SetActive(true);
                images[(int)DetectionState.Seen].SetActive(false);
                break;

            case DetectionState.Hidden: //Turns on the hidden indicator
                images[(int)DetectionState.Hidden].SetActive(true);
                images[(int)DetectionState.Heard].SetActive(false);
                images[(int)DetectionState.Seen].SetActive(false);
                break;

        }


    }

    public void ResetGame() //Resets the positions of all dynamic GameObjects in the main game
    {
        player.tf.SetPositionAndRotation(new Vector3(-4, -3, 0), new Quaternion(0, 0, 0, 0)); //Reset the player position

        //Return the enemies to their home positions
        for (int i = 0; i < enemyPawns.Count; i++)
        {
            enemyPawns[i].tf.position = enemyPawns[i].homePoint;
        }

        //Return the enemies to their home rotations
        for (int i = 0; i < enemyPawns.Count; i++)
        {
            enemyPawns[i].tf.rotation = enemyPawns[i].homeRotation;
        }

    }

    bool isSeen() //Check to see if any enemy can see the player
    {

        for (int i = 0; i < enemyControllers.Count; i++) //Run for each enemy
        {
            if (enemyControllers[i].currentState == EnemyController.EnemyState.Chase) //If the enemy is in the chase state
            {
                return true; 
            }
        }
        //If no enemies are in the chase state
        return false;
    }

    bool isHeard() //Check to see if any enemy can hear the player
    {

        for (int i = 0; i < enemyControllers.Count; i++) //Run for each enemy
        {
            if (enemyControllers[i].currentState == EnemyController.EnemyState.Search) //If the enemy is in the search state
            {
                return true;
            }
        }
        //If no enemies are in the search state
        return false;
    }

}
