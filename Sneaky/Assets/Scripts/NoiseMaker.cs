﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseMaker : MonoBehaviour
{

    public float volume = 0f; //Current volume of NoiseMaker
    public float decay = 0.01f; //Decay rate of volume

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if(volume > 0)
        {
            volume -= decay;
        }

    }
}
