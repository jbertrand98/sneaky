﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{

    private Transform tf; 
    public Transform playertf; //Player transform
    private Vector3 position;
    private Vector3 playerPosition;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); //Get this objects transform
    }

    // Update is called once per frame
    void Update()
    {

        position = tf.position;
        playerPosition = playertf.position;

        //Set this objects transform relative to the players transform
        position.x = playerPosition.x;
        position.y = playerPosition.y + 0.75f;

        tf.position = position;

    }

}
