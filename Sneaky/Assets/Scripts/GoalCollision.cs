﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalCollision : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D otherObject)
    {
        if (otherObject.gameObject.tag == "Player") //If the player collides with this object
        {
            GameManager.instance.SetGameState((int)GameManager.GameState.Win); //Set the game state to win
        }

    }
}
