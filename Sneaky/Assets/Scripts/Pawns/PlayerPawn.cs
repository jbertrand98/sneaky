﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : Pawn
{
    //NoiseMaker Component
    public NoiseMaker noisemaker;

    //Variables to be used with NoiseMaker
    public float moveVolume;
    public float maxVolume;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start(); //Calle parent start function
        noisemaker = GetComponent<NoiseMaker>(); //Get this objects NoiseMaker
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void MoveForward()
    {
        tf.Translate(Time.deltaTime * moveSpeed, 0, 0); //Move this object forward along its x-axis
        noisemaker.volume = Mathf.Max(noisemaker.volume, moveVolume); //Make noise
    }

    public override void MoveBackward()
    {
        tf.Translate(-Time.deltaTime * (moveSpeed / 2), 0, 0); //Move this object backward along its x-axis
        noisemaker.volume = Mathf.Max(noisemaker.volume, moveVolume % 2); //Make noise
    }

    public override void TurnLeft()
    {
        tf.Rotate(0, 0, Time.deltaTime * turnSpeed); //Rotate this object along its z-axis
        noisemaker.volume = Mathf.Max(noisemaker.volume, moveVolume / 2); //Make noise
    }

    public override void TurnRight()
    {
        tf.Rotate(0, 0, -Time.deltaTime * turnSpeed); //Rotate this object along its z-axis
        noisemaker.volume = Mathf.Max(noisemaker.volume, moveVolume / 2); //Make noise
    }

}
