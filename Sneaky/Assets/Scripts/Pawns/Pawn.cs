﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Base Pawn classe
public abstract class Pawn : MonoBehaviour
{
    public Transform tf;

    public float moveSpeed;
    public float turnSpeed;

    // Start is called before the first frame update
    public virtual void Start()
    {
        tf = GetComponent<Transform>(); //Get this objects transform
    }

    //Functions to be overridden in child classes
    public virtual void MoveForward()
    {

    }

    public virtual void MoveBackward()
    {

    }

    public virtual void TurnRight()
    {

    }

    public virtual void TurnLeft()
    {

    }

    public virtual int Idle()
    {
        return 0;
    }

    public virtual int Search()
    {
        return 0;
    }

    public virtual int Chase()
    {
        return 0;
    }

    public virtual int GoHome()
    {
        return 0;
    }

}
