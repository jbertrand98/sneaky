﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPawn : Pawn
{
    //Starting position and rotation of object
    public Vector3 homePoint;
    public Quaternion homeRotation;
    //Location to move towards
    public Vector3 goalPoint;
    //How far to chase the player and how close to get to goal point
    public float giveUpChaseDistance;
    public float closeEnough;
    //AISenses Component
    public AISenses senses;


    // Start is called before the first frame update
    public override void Start()
    {
        base.Start(); //Run parent start function
        senses = GetComponent<AISenses>(); //Get this objects AISenses component
        //Set home position and rotation
        homePoint = tf.position;
        homeRotation = tf.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public override int Idle() //Idle state
    {
        int state = (int)EnemyController.EnemyState.Idle; //Current state

        if (senses.CanHear(GameManager.instance.player.gameObject)) //Check if this enemy can hear the player
        {
            state = (int)EnemyController.EnemyState.Search; //Change current state
        }
        if (senses.CanSee(GameManager.instance.player.gameObject)) // Check if this enemy can see the player
        {
            state = (int)EnemyController.EnemyState.Chase; //Change the current state
        }
        return state; //Retrun the current state
        
    }

    public override int Search() //Search state
    {
        tf.Rotate(0, 0, Time.deltaTime * turnSpeed); //Rotate the object
        int state = (int)EnemyController.EnemyState.Search; //Current state
        if (senses.CanSee(GameManager.instance.player.gameObject)) //Check if this enemy can see the player
        {
            state = (int)EnemyController.EnemyState.Chase; //Change the current state
        }
        else if (Vector3.Distance(tf.position, GameManager.instance.player.tf.position) > giveUpChaseDistance) //Check if the player is too far away from this enemy
        {
            state = (int)EnemyController.EnemyState.GoHome; //Change the current state
        }
        else if (!senses.CanHear(GameManager.instance.player.gameObject)) //Check if this enemy cannot hear the player
        {
            state = (int)EnemyController.EnemyState.GoHome; //Change the current state
        }
            return state; //Return the current state
    }

    public override int Chase() //Chase state
    {

        goalPoint = GameManager.instance.player.tf.position; //Set the goal point to the players position
        MoveTowards(goalPoint); //Move towards the goal point

        int state = (int)EnemyController.EnemyState.Chase; //Current state
        if (!senses.CanSee(GameManager.instance.player.gameObject)) //Check if this enemy cannot see the player
        {
            state = (int)EnemyController.EnemyState.Search; //Change the current state
        }
        if (Vector3.Distance(tf.position, GameManager.instance.player.tf.position) > giveUpChaseDistance) //Check if the player is too far away
        {
            state = (int)EnemyController.EnemyState.GoHome; //Change the current state
        }
        return state; //Return the current state
    }

    public override int GoHome()
    {
        goalPoint = homePoint; //Set the goal point to the home position
        MoveTowards(goalPoint); //Move towards the goal point

        int state = (int)EnemyController.EnemyState.GoHome; //Current state
        if (senses.CanHear(GameManager.instance.player.gameObject)) //Check if this enemy can hear the player
        {
            state = (int)EnemyController.EnemyState.Search; //Change the current state
        }
        if (senses.CanSee(GameManager.instance.player.gameObject)) //Check if this enemy can see the player
        {
            state = (int)EnemyController.EnemyState.Chase; //Change the current state
        }
        if (Vector3.Distance(tf.position, homePoint) <= closeEnough) //Check if this object is close enough to the goal point
        {
            state = (int)EnemyController.EnemyState.Idle; //Change the current state
        }
        return state; //Return current state
    }

    public void MoveTowards(Vector3 target)
    {
        if (Vector3.Distance(tf.position, target) > closeEnough)
        {
            // Look at target
            Vector3 vectorToTarget = target - tf.position;
            tf.right = vectorToTarget;

            // Move Forward
            Move(tf.right);
        }
    }

    public void Move(Vector3 direction)
    {
        // Move in the direction passed in, at speed "moveSpeed"
        tf.position += (direction.normalized * moveSpeed * Time.deltaTime);
    }

}
