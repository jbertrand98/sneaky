﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{

    private Transform tf;
    public Transform playertf;
    private Vector3 position;
    private Vector3 playerPosition;


    // Start is called before the first frame update
    void Start()
    {
        tf = this.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        position = tf.position;
        playerPosition = playertf.position;

        position.y = playerPosition.y;

        tf.position = position;
    }
}
